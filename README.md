# FILM GEEK 🎬

# Welcome to FILM GEEK!

FilmGeek is a mobile application that allows you to explore, discover, and enjoy a wide variety of movies from different genres. Looking for the latest movie releases? FilmGeek has something for every movie enthusiast.

## Key Features:

➡️ Explore Movies: Browse through a wide selection of movies organized by popularity.
➡️ Movie Details: Get detailed information about each movie, including its synopsis and release date.

## Technologies Used:

➡️ React
➡️ React Navigation
➡️ Movie API (The Movie Database API)

I HOPE YOU ENJOY IT! 🚀
