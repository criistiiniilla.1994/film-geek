import ContextMovieCard from "../components/ContextMovieCard";
import Footer from "../components/Footer";
import Header from "../components/Header";
import "../index.css";

const LandingPage = () => {
  return (
    <>
      <Header />
      <ContextMovieCard />
      <Footer />
    </>
  );
};
export default LandingPage;
