import { useState, useEffect } from "react";
import { useParams, NavLink } from "react-router-dom";
import Modal from "react-modal"; // Importa react-modal
import Videos from "../components/Videos"; // Importa tu componente Videos
import "../styles/movieDetails.css";
import "../styles/header.css";

// Estilo del modal (puedes ajustarlo según lo que desees)
Modal.setAppElement('#root'); // Esto es necesario para accesibilidad

const MovieDetails = () => {
  const [movie, setMovie] = useState([]);
  const [genero, setGenero] = useState([]);
  const [loading, setLoading] = useState(true);
  const [fechaDesdeBD, setFechaDesdeBD] = useState(null);
  const [modalIsOpen, setModalIsOpen] = useState(false); // Estado para controlar el modal
  const { movieId } = useParams();

  useEffect(() => {
    const fetchDetails = async () => {
      try {
        const res = await fetch(
          `${import.meta.env.VITE_API_URL}${movieId}?language=es-ES`,
          {
            headers: {
              accept: 'application/json;',
              Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmMDA2NjUwYjMxODM3YTIwYTA3ZGI3OGU0ZjllNDAwNSIsIm5iZiI6MTcyODQ5Nzg4Ny4wMjQzMzYsInN1YiI6IjY1Y2E1OTQ4OTQ1MWU3MDE4NDdkYjZiZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.amRDlnGWKDj53T8hFSdG4EcHj-L45RTugRqZ2P0PYRw'
            },
          },
        );
        if (!res.ok) {
          throw new Error(`HTTP Error: ${res.status}`);
        }
        const body = await res.json();
        setMovie(body);
        setGenero(body.genres[0]);
        setFechaDesdeBD(body.release_date);
      } catch (error) {
        console.error("Error fetching entries:", error);
        throw error;
      } finally {
        setLoading(false);
      }
    };
    fetchDetails();
  }, [movie, movieId, fechaDesdeBD]);

  return (
    <>
      <NavLink className="nav-image" to={"/"}>
        <div class="icon-container">
          <img class="icon" src="/cine.png" alt="icon" />
          <span class="home-text">Home</span>
        </div>
        <h1 className="title-details">Detalles</h1>
      </NavLink>
      {loading ? (
        <p>Loading...</p>
      ) : (
        <>
          <div className="detailsContainer">
            <div className="col movieDetails">
              <h2 className="title-film">{movie.title}</h2>
              <div className="container-image">
                {!movie.backdrop_path ? (
                  <img
                    className="col-movieImage image"
                    src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
                    alt={movie.title}
                  />
                ) : (
                  <img
                    className="col movieImage image"
                    src={`https://image.tmdb.org/t/p/w500${movie.backdrop_path}`}
                    alt={movie.title}
                  />
                )}
              </div>
              {/* Botón que abre el modal */}
              <button onClick={() => setModalIsOpen(true)} className="link-video">
                Ver Trailer
              </button>

              {/* Modal */}
              <Modal
                isOpen={modalIsOpen}
                onRequestClose={() => setModalIsOpen(false)}
                contentLabel="Trailer Modal"
                className="modal"
                overlayClassName="overlay"
              >
                <button onClick={() => setModalIsOpen(false)}>x</button>
                <Videos />
              </Modal>

              <p className="genre">
                <strong className="genre">Género:</strong> {genero.name}
              </p>
              <p className="date">
                  <strong className="date">Fecha de estreno:</strong> {" "}
                  {new Date(fechaDesdeBD).toLocaleDateString("es-ES", {
                    day: "2-digit",
                    month: "2-digit",
                    year: "numeric",
                  })}
              </p>
              {movie.overview && (
                <p className="synopsis">
                  <strong className="synopsis">Sinopsis:</strong> {movie.overview}
                </p>
              )}
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default MovieDetails;
