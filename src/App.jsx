import MyRoutes from "./Routers/Routes";
const App = () => {
  return (
    <>
      <MyRoutes />
    </>
  );
};

export default App;
