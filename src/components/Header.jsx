import "../styles/header.css";
import { NavLink } from "react-router-dom";

const Header = () => {
  return (
    <header>
      <NavLink className="nav-image" to={"/"}>
        <div className="icon-container">
          <img className="icon" src="/cine.png" alt="icon" />
          <span className="home-text">Home</span>
        </div>
        <h1 className="title-header">Peliculas en tendencia </h1>
      </NavLink>
    </header>
  );
};
export default Header;
