import { useState, useEffect } from "react";
import MovieCards from "./MovieCards";
import "../styles/contextMovieCard.css";

const ContextMovieCard = () => {
  const [movie, setMovie] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchMovies = async () => {
      try {
        const res = await fetch(
          `${import.meta.env.VITE_API_URL}popular?language=es-ES&page=1`,
          {
            headers: {
              accept: 'application/json;',
              Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmMDA2NjUwYjMxODM3YTIwYTA3ZGI3OGU0ZjllNDAwNSIsIm5iZiI6MTcyODQ5Nzg4Ny4wMjQzMzYsInN1YiI6IjY1Y2E1OTQ4OTQ1MWU3MDE4NDdkYjZiZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.amRDlnGWKDj53T8hFSdG4EcHj-L45RTugRqZ2P0PYRw'
            },
          },
        );

        if (!res.ok) {
          throw new Error(`HTTP Error: ${res.status}`);
        }

        const body = await res.json();

        setMovie(body.results);
      } catch (error) {
        console.error("Error fetching entries:", error);
        throw error;
      } finally {
        setLoading(false);
      }
    };
    fetchMovies();
  }, []);

  return loading ? (
    <p>Loading...</p>
  ) : (
    <ul className="container">
      {movie.map((item) => (
        <MovieCards key={item.id} film={item} />
      ))}
    </ul>
  );
};
export default ContextMovieCard;
