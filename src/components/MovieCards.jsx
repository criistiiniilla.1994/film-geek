import "../styles/movieCards.css";
import { Link } from "react-router-dom";

const MovieCards = ({ film }) => {
  const imageUrl = `https://image.tmdb.org/t/p/w500${film.poster_path}`;
  return (
    <>
      <Link to={`/movie/${film.id}`}>
          <li className="moviecard">
            <img
              className="movieimage"
              src={imageUrl}
              alt="film.title"
              width={230}
              height={345}
            />
            <p className="title">{film.title}</p>
          </li>
        
      </Link>
    </>
  );
};
export default MovieCards;
