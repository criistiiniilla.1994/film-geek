import { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import YouTube from "react-youtube";
import { useParams } from "react-router-dom";
import "../styles/video.css";
import "../styles/header.css";

function Videos() {
  const [videos, setTrailer] = useState([]);
  const [loading, setLoading] = useState(true);
  const { movieId } = useParams();
  console.log(videos);
  useEffect(() => {
    const fetchTrailer = async () => {
      try {
        const res = await fetch(
          `https://api.themoviedb.org/3/movie/${movieId}/videos?language=en-US`,
          {
            headers: {
              accept: 'application/json;charset="UTF-8"',
              Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmMDA2NjUwYjMxODM3YTIwYTA3ZGI3OGU0ZjllNDAwNSIsIm5iZiI6MTcyODQ5Nzg4Ny4wMjQzMzYsInN1YiI6IjY1Y2E1OTQ4OTQ1MWU3MDE4NDdkYjZiZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.amRDlnGWKDj53T8hFSdG4EcHj-L45RTugRqZ2P0PYRw',
            },
          },
        );
        if (!res.ok) {
          throw new Error(`HTTP ERROR: ${res.status}`);
        }
        const body = await res.json();
        const trailer = body.results.find(
          (video) => video.type === "Trailer" || video.type === "Teaser",
        );
        setTrailer(trailer);
      } catch (error) {
        console.error("Error fetching entries:", error);
        throw error;
      } finally {
        setLoading(false);
      }
    };
    fetchTrailer();
  }, [movieId]);

  return (
    <div>
      {!movieId ? <img src="/error404" /> : null}
      {loading ? (
        <p>Loading...</p>
      ) : (
        <div className="video-container">
          <YouTube className="video" videoId={videos.key} />
        </div>
      )}
    </div>
  );
}

export default Videos;
